// uscheme.cpp

#include <iostream>
#include <sstream>
#include <stack>
#include <string>
#include <unistd.h>
#include <ctype.h>

using namespace std;

// Globals ---------------------------------------------------------------------

bool BATCH = false;
bool DEBUG = false;

// Structures ------------------------------------------------------------------

struct Node {
    Node(string value, Node *left=nullptr, Node *right=nullptr);
    ~Node();

    string value;
    Node * left;
    Node * right;

    friend ostream &operator<<(ostream &os, const Node &n);
};

// Code I have added to the file

Node::Node(string str, Node * left, Node * right) {
    this->value = str;
    this->left = left;
    this->right = right;
}

Node::~Node() {
    delete left;
    delete right;
}

ostream &operator<<(ostream &os, const Node &n) {
    os << "(Node: value=" << n.value;
    if (n.left)
        os << ", left=" << *n.left << ", ";
    if (n.right)
        os << "right=" << *n.right;
    os << ")";
    return os;
}

// Parser ----------------------------------------------------------------------

string parse_token(istream &s) {
    string token;
    if(isspace(s.peek())){			//skip whitespace
    	token = s.get();
    	token = parse_token(s);
    }
    else if(!isdigit(s.peek()))	//not a digit (either a parenthese or operator)
    	token = s.get();
    else if(isdigit(s.peek())){	//a digit
    	token = s.get();	//get the digit
    	while(isdigit(s.peek())){	//while the next input is a digit, 
    		string nextDig(1,s.get()); //convert char to string
    		token = token + nextDig;//add it on to the end of the token
    	}
    }
    return token;
}

Node *parse_expression(istream &s){
     string token = parse_token(s);
     Node * left = nullptr,* right = nullptr;
     if(token=="" || token==")") //end of an expression
     	return nullptr;
     if(token=="("){	//start of an expression
     	token = parse_token(s); //operator or another (
     	left = parse_expression(s); //points to the left node
     	if(left)
     		right = parse_expression(s); //points to the right node
     	if(right)
     		parse_token(s);
     }
     return new Node(token, left, right);
}

// Interpreter -----------------------------------------------------------------

void evaluate_r(const Node *n, stack<int> &s) {
    // evaluates a nodes children before itself, modeling a post-order binary tree
    // traversal.
    if (n->left)
        evaluate_r(n->left, s);
    if (n->right)
        evaluate_r(n->right,s);
    // if the value of the node is a number, it gets pushed onto the stack.
    // if the value of the node is an operator, it gets performed on the top
    // two values of the stack, and the result is then pushed back onto the stack
    // for later use
            
    if (n->value == "+") {
        int temp = s.top();
        s.pop();
        temp += s.top();
        s.pop();
        s.push(temp);
    }
    else if (n->value == "-") {
        int temp = s.top();
        s.pop();
        temp = s.top() - temp;
        s.pop();
        s.push(temp);
    }
    else if (n->value == "/")
    {
        int temp = s.top();
        s.pop();
        temp = s.top() / temp;
        s.pop();
        s.push(temp);
    }
    else if (n->value == "*") {
        int temp = s.top();
        s.pop();
        temp *= s.top();
        s.pop();
        s.push(temp);
    }
    else 
        s.push(stoi(n->value));
 
}

int evaluate(const Node *n) {
    // creates a stack of ints that is then used to evaluate the binary tree
    // based on its configuration from the parser
    stack<int> working;
    evaluate_r(n, working);
    int ret = working.top();
    return ret;
}

// Main execution --------------------------------------------------------------

int main(int argc, char *argv[]) {
    string line;
    int c;

    while ((c = getopt(argc, argv, "bdh")) != -1) {
        switch (c) {
            case 'b': BATCH = true; break;
            case 'd': DEBUG = true; break;
            default:
                cerr << "usage: " << argv[0] << endl;
                cerr << "    -b Batch mode (disable prompt)"   << endl;
                cerr << "    -d Debug mode (display messages)" << endl;
                return 1;
        }
    }

    while (!cin.eof()) {
        if (!BATCH) {
            cout << ">>> ";
            cout.flush();
        }

        if (!getline(cin, line)) {
            break;
        }

        if (DEBUG) { cout << "LINE: " << line << endl; }

        stringstream s(line);
        Node *n = parse_expression(s);
        if (DEBUG) { cout << "TREE: " << *n << endl; }

        cout << evaluate(n) << endl;

        delete n;
    }

    return 0;
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
