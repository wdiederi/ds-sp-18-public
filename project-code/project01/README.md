# Data Structures Project 01 - Arithmetic Scheme Interpreter

### Group members:
- Will Diederich (wdiederi)
- Patrick Bouchon (pbouchon)

### Questions

1. What is the complexity of your interpreter in terms of Big-O notation? Explain exactly what N is in this case and the asymptotic behavior of your interpreter for when N grows. Moreover, be sure to consider the impact of using a stack during evaluation.
	* The interpreter is O(N), because the evalute_r function is of constant time complexity and is run once for every node in the tree. N is the 
	number of nodes in the abstract syntax tree. As N grows, the interpreter grows in time linearly. Using a stack only adds linear time constraints to 
	the program, and does not necesarily increase the time complexity.

2. Compare the resource usage of your two versions of the Scheme interpreter (i.e. uscheme and uschemeSmart) by determining:
    * Which executable is larger?
    * Which executable uses more memory?
    * By using the du command, we determined that the uschemeSmart executable was larger. At 136KB compared to 110KB, it is apparent that the use of smart pointers takes up more storage than manual memory management would. In terms of memory usage, valgrind determined that uschemeSmart allocates more memory when the same commands are run in each version of the program. This makes sense, as an automated memory management system is destined to use more memory and storage than a system designed specifically to manage for a program.  

3. Which implementation do you prefer: the one with manual memory management, or the one with smart pointers? Explain the trade-offs and your reasoning.
    * Automatic memory management may make one's life easier, but it comes at a cost. In small programs, this cost is a negligible performance drop off, but as program size increases I can forsee considerable performance issues due to memory management. In general, the manual memory management is more difficult to implement, but if done carefully, yields a program with more control over its functionality. When writing code, it is important to attempt to do things while utilizing as few resources as possible so that a machine can function better while running your program. Thus, I would reccommend that the method with manual memory management be used to make code more readable and provide more control to the user.  

4. Real world Scheme interpreters such as Racket and Guile support arbitrary length expressions. For instance, instead of binary operations such as (+ 1 2), these implementations support (+ 1 2 3 4 5). What changes to your interpreter would you need to make support such syntax? Explain how the structures, parser, and interpreter would need to be modified.
	* To support a syntax which allows for multiple numbers to be affected by the same operator, our structures would need to allow for multiple 
	branching nodes, not simply a left and a right. Similarly, parse expression would need to account for any number of branching nodes, not necesarily 
	just left and right. To account for multiple digits following the same opperator, our interpreter would need to be rewritten to perform the given 
	operation on the correct number of values, not simply the next two values on the stack. One way to account for this would be to have a data value on 
	any node stating the number of children, x, and that number x could be used in the interpreter to tell how many values to take off of the stack and 
	use with the given operator.
	
### Individual Contributions:

List the contributions of each member along with a score out of 10 that all members agree with.
	
    - Patrick Bouchon: My main job in this project involved writing both the parse_token and parse_expression functions. I was in charge of both writing 
	the initial versions of these functions, and debugging them to allow the program to compile and ensure no memory errors were encounted when running 
	the program under any scenario. Additionally, I answered questions 1 and 4. (10/10)
    - Will Diederich: My main job in this project involved programming the evaluate and evaluate_r functions. In addition to debugging my portion of the
    code, I also converted the program to a program that used smart pointers. In terms of answering the above questions, I mainly worked on questions 2 
    and 3, which discuss the differences in memory usage/management methods.
