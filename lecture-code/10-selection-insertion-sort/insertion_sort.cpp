// insertion_sort.cpp

#include <algorithm>
#include <iostream>

using namespace std;

#define DUMP_ARRAY(p, a, n) \
    cout << p; for_each(a, a + n, [](int x) { cout << x << " "; }); cout << endl;

// Insertion sort:
//
//  For each slot in the array, starting from the 2nd element and moving to the
//  end of the array
//
//	From current element to front of array, shift element as long as
//	current element < next element.
//
void insertion_sort(int a[], size_t n) {
    if (n <= 1)
    	return;

    for (size_t back = 1; back < n; back++) {
    	for (size_t i = back; i > 0 && a[i] < a[i - 1]; i--) {
	        swap(a[i], a[i - 1]);
	        DUMP_ARRAY(" ", a, n);
	    }

	    DUMP_ARRAY("", a, n);
    }
}

int main(int argc, char *argv[]) {
    int numbers[] = {5, 4, 7, 0, 1};

    cout << "Unsorted:" << endl;
    DUMP_ARRAY("", numbers, 5);

    insertion_sort(numbers, 5);

    cout << "Sorted:" << endl;
    DUMP_ARRAY("", numbers, 5);

    return 0;
}
