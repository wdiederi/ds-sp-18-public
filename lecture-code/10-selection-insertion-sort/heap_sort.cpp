// heap_sort.cpp

#include <algorithm>
#include <iostream>

using namespace std;

#define DUMP_ARRAY(p, a, n) \
    cout << p; for_each(a, a + n, [](int i) { cout << i << " "; }); cout << endl;

// heap sort:
//
//  Convert container into a heap (heapify)
//
//  While there are still items unsorted
//	Swap first element (max) with last unsorted
//	Reheapify-down the heap portion
//	Decrement unsorted
void heap_sort(int a[], size_t n) {
    if (n <= 1)
    	return;

    make_heap(a, a + n);
    DUMP_ARRAY("", a, n);

    for (size_t unsorted = n; unsorted > 1; unsorted--) {
    	pop_heap(a, a + unsorted); // Swap first and unsorted and reheapify down
	DUMP_ARRAY("", a, n);
    }
}

int main(int argc, char *argv[]) {
    int numbers[] = {5, 4, 7, 0, 1};

    cout << "Unsorted:" << endl; 
    DUMP_ARRAY("", numbers, 5);

    heap_sort(numbers, 5);
    
    cout << "Sorted:" << endl;
    DUMP_ARRAY("", numbers, 5);

    return 0;
}
