// stack_vector

#include <stack>
#include <list>

using namespace std;

const int N = 1<<28;

int main(int argc, char const *argv[]) {

    stack<int, list<int>> s;

    // fill the stack
    for (int i = 0; i < N; i++) {
        s.push(i);
    }

    // empty the stack
    while (!s.empty()) {
        s.pop();
    }

    return 0;
}
