#pragma once

#include <stack>

template <typename T>
class max_stack {
    std::stack<T> values;
    std::stack<T> max_values;

    public:
	bool empty() const { return values.empty(); }
	const T& top() const { return values.top(); }
	void push(const T& value) {
	    values.push(value);

	    if (max_values.empty() || value > max_values.top()) {
	    	max_values.push(value);
	    } else {
	    	max_values.push(max_values.top());
	    }
	}

	void pop() {
	    values.pop();
	    max_values.pop();
	}

	const T& max() const { return max_values.top(); }
};
