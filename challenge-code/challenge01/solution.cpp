// Challenge 01: Rotating Arrays

// Main Execution

#include <iostream>
#include <stdio.h>
#include <string>

using namespace std;

void displayArray(int[], int);
void rotateLeft(int arr[], int num, int sz);
void rotateRight(int arr[], int num, int sz);

int main(int argc, char *argv[]) {
    
    int n,r;
    char d;   
    while (cin >> n >> r >> d) {
        int arr[n];
        for (int ix = 0; ix < n; ix++)
            cin >> arr[ix];

        if (d == 'L')
            rotateLeft(arr, r, n);
        if (d == 'R')
            rotateRight(arr,r,n);
    }
    return 0;
}

void displayArray(int arr[], int sz) {
    for (int x = 0; x < sz; x++) {
        cout << arr[x];
        if (x != sz-1)
            cout << ' ';
    }
    cout << endl;
}


void rotateLeft(int arr[], int num, int sz) {
    int temparr[sz];
    for (int x = 0; x < sz; x++) {
        int temploc = x;
        temploc -= num;
        while (temploc < 0)
            temploc += sz;
        temparr[temploc] = arr[x];
    }
    arr = temparr;
    displayArray(temparr, sz);
}

void rotateRight(int arr[], int num, int sz) {
    int temparr[sz];   
    for (int x = 0; x < sz; x++) {
        int temploc = x;
        temploc += num;
        temparr[temploc%sz] = arr[x];
    } 
   
    displayArray(temparr, sz);
}
// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
