// Challenge 03: Closest Numbers

#include <algorithm>
#include <climits>
#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

// Main Execution

void readin(int arr[], int size) {
    for (int i = 0; i < size; i++)
        cin >> arr[i];
}

int main(int argc, char *argv[]) {
    int size;
    while (cin >> size) {
        int numbers[size];
        vector<int> sorted;
        readin(numbers, size);  
        int small = -1;
        for (int x = 0; x < size; x++) {
            for (int y = x+1; y < size; y++) {
                if (abs(numbers[x] - numbers[y]) < small || small == -1)
                    small = abs(numbers[x] - numbers[y]);
            }
        }
        
        for (int x = size-1; x >=0; x--) {
            for (int y = x-1; y >= 0; y--) {
                if (abs(numbers[x] - numbers[y]) == small) {
                    sorted.push_back(numbers[x]);
                    sorted.push_back(numbers[y]);
                }
            }
        }   
        sort(sorted.begin(), sorted.end());

        bool first = true;
        for (auto it = sorted.begin(); it != sorted.end(); it++) {
            if (!first)
                cout << " ";
            first =false;
            cout << *it;
        }
        cout << endl;
    }        

    return EXIT_SUCCESS;
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
