// Challenge 02: Adding List-Based Integers

#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <string>

using namespace std;

const int NITEMS = 10;

// List declarations

template <typename T>
class List {
    protected:
        struct Node {
            T data;
            Node *next;
        };
        Node *head;
    
    public:
        typedef Node * iterator;    
        List() : head(nullptr) { }; 
        iterator front() { return head; }
        
        size_t size() const;
        void add_front(const T &data);
        void push_back(const T &data);
        void print();
        void clear();
};

// List implementation

template <typename T>
size_t List<T>::size() const{
    size_t size = 0;

    for (Node* curr = head; curr != nullptr; curr->next) {
        size++;
    }

    return size;
}

template <typename T>
void List<T>::add_front(const T &data) {
    // for an empty list
    if (head == nullptr)
    {
        head = new Node {data, nullptr};
        return;
    } 

    head = new Node {data, head};
}

template <typename T>
void List<T>::push_back(const T &data) {
    if (head == nullptr) {
        head = new Node{data, nullptr};
        return;
    }

    Node *curr = head;
    Node *tail = head;

    while (curr) {
        tail = curr;
        curr = curr->next;
    }
    tail->next = new Node{data, nullptr};
}

template <typename T>
void List<T>::print() {
    string output = "";  

    if (head == nullptr)
        return;
    
    Node *curr = head;

    while (curr) {
        output.insert(0,to_string(curr->data));
        curr = curr->next;
    }
    cout << output << endl;
}

template <typename T>
void List<T>::clear() {
    Node *curr = head;
    Node *check = head;
    while (check) {
        check = curr->next;
        delete curr;
        curr = check;
    }
    head = nullptr;
}


// Other functions


void read(string str, List<int> &list)
{
    for (unsigned int x = 0; x < str.length(); x++)
        list.add_front(str[x]-'0');   
}

List<int> add (List<int> list1, List<int> list2) {
    List<int> sum;

    List<int>::iterator it1 = list1.front();
    List<int>::iterator it2 = list2.front(); 

    int temp, carry = 0;
    while (it1 || it2) {
        
        if (it1 && it2) {  
            temp = it1->data + it2->data + carry;
            if (temp >= 10) {
                temp -= 10;
                carry = 1;
            }
            else carry = 0;
            sum.push_back(temp);
            it1 = it1->next;
            it2 = it2->next;
        }
        else if (it1) {
            temp = it1->data + carry;
            if (temp >= 10) {
                temp -= 10;
                carry = 1;
            }
            else carry = 0;
            sum.push_back(temp);
            it1 = it1->next;
        }
        else if (it2) {
            temp = it2->data + carry;
            if (temp >= 10) {
                temp -= 10;
                carry = 1;
            }
            else carry = 0;
            sum.push_back(temp);
            it2 = it2->next;
        }
        
    }
    if (carry == 1)
        sum.push_back(carry); 
    return sum;     
}


// Main Execution

int main(int argc, char *argv[]) {
       
    List<int> num1, num2, sum;
    string str1, str2;

    while (cin >> str1 >> str2)
    {
        read(str1, num1);
        read(str2, num2);
        sum = add(num1,num2);
        sum.print();
        num1.clear();
        num2.clear();
        sum.clear();
    }
    return 0;
}
// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
