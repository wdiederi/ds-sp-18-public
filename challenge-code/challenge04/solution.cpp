// Challenge 04: Card Rank

#include <string>
#include <vector>
#include <cstdlib>
#include <iostream>

using namespace std;

// struct delcaration
struct Player {
    string name;
    int number;
    int suit;
    Player(string str, int a, char b) : name(str), number(a), suit(b) {}
};

vector<Player> readInPlayers (int);
vector<Player> sort(vector<Player>, int);
void displayNames(vector<Player>);


// Main Execution

int main(int argc, char *argv[]) {
    int size;
    vector<Player> players;
    bool first = true;
    while (cin >> size) {
        if (!first)
            cout << endl;
        first = false;
        players = readInPlayers(size);
        players = sort(players, size);
        displayNames(players);
        players.clear();
    }
    return EXIT_SUCCESS;
}

vector<Player> readInPlayers (int sz) {
    vector<Player> players;
    string str;
    int x;
    char  d;
    string c;
    int s;
    for ( int ix = 0; ix < sz; ix++) {
        cin >> str;
        cin >> c;
        if (c == "J") x = 11;
        else if (c == "Q") x = 12;
        else if (c == "K") x = 13;
        else if (c == "A") x = 14;
        else x = stoi(c);

        cin >> d;
        switch (d) {
            case 'C' : s = 1;
                break;
            case 'D' : s = 2;
                break;
            case 'H' : s = 3;
                break;
            case 'S' : s = 4;
                break;
        }
        Player * p = new Player(str, x, s);
        players.push_back(*p);
        delete p;
    }
    return players;
}

vector<Player> sort(vector<Player> unsorted, int sz) {
    vector<Player> sorted;
    vector<Player>::iterator highest;
    while (unsorted.size() > 0) {
        highest = unsorted.begin();
        for (auto it = unsorted.begin(); it != unsorted.end(); it++) {
            if ((*it).number > (*highest).number) 
                highest = it;
            else if ((*it).number == (*highest).number)
                if ((*it).suit > (*highest).suit)
                    highest = it;
        }
        sorted.push_back(*highest);
        unsorted.erase(highest);
    }   
    return sorted;
}

void displayNames(vector<Player> v) {
    for (auto it = v.begin(); it != v.end(); it++) {
        if (it != v.begin())
            cout << ", ";   
        cout << (*it).name;  
    }
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
